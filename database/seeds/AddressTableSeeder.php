<?php

use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = App\User::all();

    	$users->each(function($user){
    		$addresses = factory(App\Address::class,2)->make([
    						'address_to' => $user->name
    						]);
    		$addresses->first()->setDefault();
    		$user->addresses()->saveMany($addresses);
    	});
        
    }
}
