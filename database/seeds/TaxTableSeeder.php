<?php

use Illuminate\Database\Seeder;

class TaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Tax::class,1)->create([
			'name' => "GST",
	        'percentage' => 10.00,
	        'active' => 1
        ]);
    }
}
