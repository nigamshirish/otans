<?php

use Illuminate\Database\Seeder;

class LookupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$Lookup = [
  			[	"name" => "OrderStatus",
  				"description" => "Order Status",
  				"items" => [
  					["name" => "New","description" => "New Order"],
  					["name" => "Processed","description" => "Order processed"],
  					["name" => "Scheduled","description" => "Scheduled for delivery"],
  					["name" => "Cancelled","description" => "Order cancelled"]
  				]
  			],
  			[	"name" => "DeliveryStatus",
  				"description" => "Delivery Status",
  				"items" => [
  					["name" => "InRoute","description" => "To be delivered next"],
  					["name" => "Delivered","description" => "Delivered to customer"],
  					["name" => "NotHome","description" => "Customer not home"]
  				]
  			],
    	];

  		collect($Lookup)->each(function($lkp) {

  			$lookup = new App\Lookup;
  			$lookup->name = $lkp['name'];
  			$lookup->description = $lkp['description'];

  			$items = collect($lkp['items'])->transform(function($itm){
  				$lookupItem = new App\LookupItem;
  				$lookupItem->name = $itm['name'];
  				$lookupItem->description = $itm['description'];
  				return $lookupItem;
  			});

  			$lookup->save();
  			$lookup->lookupItems()->saveMany($items);
  		});
    }
}
