<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = App\Product::all();
        $customers = App\User::with('addresses')->get();
        $orders = factory(App\Order::class,5)->make();

        $orders->each(function(&$order) use($customers,$products){
            $customer = $customers->random();

            $defaultAddress = $customer->addresses->where('is_default', 1);
            $defaultAddress->all();
        	$orderItems = $this->makeOrderItems($products);
        	$order->customer_id = $customer->id;
        	$order->order_date  = Carbon::today();
            $order->billing_address_id = $defaultAddress->first()->id; 
            $order->delivery_address_id = $defaultAddress->first()->id; 
        	$order->save();
        	$order->orderItems()->saveMany($orderItems);
        });

        $orders->each(function($o) {
        	$o->updateTaxTotal()
        	  ->updateSubTotal()
        	  ->updatePayableAmount()
        	  ->save();
        });
    }
    function makeOrderItems($products)
    {
        $orderItems = factory(App\OrderItem::class,5)->make();
        $orderItems->each(function(&$oi) use($products){
        	$product = $products->random();
        	$oi->product_id = $product->id;
        	$oi->quantity = 2;
        	$oi->tax_amount = $product->tax_amount * $oi->quantity;
        	$oi->sub_total = $product->list_price * $oi->quantity;
        });
        return $orderItems;
    }
}
