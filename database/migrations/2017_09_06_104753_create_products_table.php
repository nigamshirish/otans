<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            // Product Fields

            // Custom field to put in product code
            $table->string('product_code')->nullable();
            // Product name
            $table->string('product_name');
            // Product description
            $table->string('product_description')->nullable();
            // Product unit price, with or without tax component included
            $table->decimal('unit_price',8,2)->default(0.00);
            
            $table->decimal('tax_amount',8,2)->default(0.00);

            // Calculated field based on tax rate and tax applicability
            $table->decimal('list_price',8,2)->default(0.00);

            // Tax Fields
            $table->integer('tax_id')->nullable();
            // Determine if the tax applicable on the product
            $table->boolean('tax_applicable')->default(0);

            $table->boolean('active')->default(0);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
