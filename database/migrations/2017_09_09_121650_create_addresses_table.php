<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            // Customer Id 
            $table->integer('customer_id');
            // Short name for the address to remember
            $table->string('address_name');
            // Name to appear on order
            $table->string('address_to')->nullable();
            // Mobile / phone number to contact the customer
            $table->string('address_contact')->nullable();
            // Address Line 1
            $table->string('address_line_1');
            // Address Line 2
            $table->string('address_line_2')->nullable();
            // Address City or Suburb
            $table->string('address_city');
            // Address Postcode
            $table->string('address_postcode');
            // Address State
            $table->string('address_state');
            // Address Country
            $table->string('address_country')->default('AU');
            // Is default address
            $table->string('is_default')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
