<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            
            $table->increments('id');

            // Customer Id
            $table->integer('customer_id');
            
            // ------------- //
            // Order Fields  //
            // ------------- //
            
            $table->date('order_date');
            $table->integer('order_status_id')->nullable();
            $table->boolean('order_paid')->default(0);
            $table->decimal('sub_total',8,2)->default(0.00);
            $table->decimal('tax_total',8,2)->default(0.00);
            $table->decimal('payable_amount',8,2)->default(0.00);
            $table->string('order_channel')->nullable();

            // --------------- //
            // Billing Fields  //
            // --------------- //

            // Billing Address Id
            $table->integer('billing_address_id')->nullable();
            // Adddress to customer 
            $table->string('bill_to')->nullable();
            // Contact number for this bill
            $table->string('billing_contact')->nullable();
            // Billing address line 1
            $table->string('billing_address_1')->nullable();
            // Billing address line 2
            $table->string('billing_address_2')->nullable();
            // Billing address suburb or city
            $table->string('billing_address_city')->nullable();
            // Billing address state
            $table->string('billing_address_state')->nullable();
            // Billing address postcode
            $table->string('billing_address_postcode')->nullable();
            // Billing address country
            $table->string('billing_address_country')->nullable();
            
            // --------------- //
            // Delivery Fields //
            // --------------- //

            // Delivery Address Id
            $table->integer('delivery_address_id')->nullable();
            // Adddress to customer 
            $table->string('deliver_to')->nullable();
            // Contact number for this delivery
            $table->string('delivery_contact')->nullable();
            // Delivery address line 1
            $table->string('delivery_address_1')->nullable();
            // Delivery address line 2
            $table->string('delivery_address_2')->nullable();
            // Delivery address suburb or city
            $table->string('delivery_address_city')->nullable();
            // Delivery address state
            $table->string('delivery_address_state')->nullable();
            // Delivery address postcode
            $table->string('delivery_address_postcode')->nullable();
            // Delivery address country
            $table->string('delivery_address_country')->nullable();
            // Delivery date
            $table->datetime('delivery_date')->nullable();
            // Delivery status
            $table->integer('delivery_status_id')->nullable();
            
            // Store customer comments
            $table->text('comments')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
