<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {

    $unitPrice = $faker->randomFloat(2,100,500);
    $listPrice = $unitPrice; 
    return [
        'product_name' => $faker->randomElement([
			        		'LPG - 45KG',
			        		'LPG - 40KG',
			        		'LPG - 35KG',
			        		'HELIUM - 5KG'
        				]),
        'product_description' => 'Gas Product',
        'unit_price' => $unitPrice,
        'list_price' => $listPrice
    ];
});
