<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'address_name'		=> $faker->randomElement([ 'Home','Business' ]),
        'address_to'		=> $faker->name(),
        'address_line_1'	=> $faker->streetAddress(),
        'address_city'		=> $faker->city(),
        'address_state'		=> $faker->state(),
        'address_postcode'	=> $faker->postcode(),
        'address_country'   => $faker->country(),
        'is_default'        => 0
    ];
});
