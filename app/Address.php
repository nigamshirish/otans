<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [ 
		'address_name', 'address_to',
		'address_line_1','address_line_2','address_city','address_state',
		'address_postcode','address_country','is_default'
	 ];
	
	protected $casts = ['is_default' => 'boolean'];

	public function customer()
	{
		return $this->belongsTo(User::class,'customer_id','id');
	}
	public function setDefault()
	{
		$this->is_default = 1;
	}
	public function removeDefault()
	{
		$this->is_default = 0;
	}

	public function __toString()
	{
		$address = '';
		$address .= $this->address_line_1.', '; 
		$address .= is_null($this->address_line_2) ? '' : $this->address_line_2;
		$address .= $this->address_city .' '. $this->address_state .', ' .$this->address_postcode .' ';
		$address .= $this->address_country;
		return $address;
	}
	
}
