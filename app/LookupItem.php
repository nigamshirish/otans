<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LookupItem extends Model
{
    protected $fillable = [ 'name', 'description' ];

    public function Lookup()
    {
    	return $this->belongsTo(Lookup::class);
    }
}
