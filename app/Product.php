<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [ 
		'product_code', 'product_name','product_description',
		'unit_price','tax_amount','list_price',
		'tax_id','tax_applicable','active'
	 ];
	protected $casts = [ 
		'unit_price' => 'decimal','tax_amount' => 'decimal', 
		'list_price' => 'decimal',
		'tax_applicable' => 'boolean','active' => 'boolean'
	];

    public function deactivate()
    {
    	$this->active = 0;
    }

    public function activate()
    {
    	$this->active = 1;
    }
	public function removeTaxComponent()
	{
		$this->tax_id = null;
		$this->tax_amount = 0.00;
		$this->tax_applicable = 0;
		$this->updateListPrice();
	}
	public function applyTaxComponent(Tax $tax)
	{
		$this->tax_id = $tax->id;
		$this->tax_amount = $this->calculateTaxAmount($tax);
		$this->tax_applicable = 1;
		$this->updateListPrice();
	}

    public function calculateTaxAmount(Tax $tax)
    {
    	$amt = $this->unit_price * ($tax->percentage/100);
    	return round($amt,2);
    }
    public function updateListPrice()
    {
    	$this->list_price = $this->unit_price + $this->tax_amount;
    }
}
