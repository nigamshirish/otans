<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lookup extends Model
{
    protected $fillable = [ 'name', 'description' ];

    public function lookupItems()
    {
    	return $this->hasMany(LookupItem::class);
    }
}
