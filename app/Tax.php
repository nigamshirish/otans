<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
	protected $fillable = [ 'name', 'percentage','active' ];
	protected $casts = [ 'percentage' => 'decimal','active' => 'boolean'];

    public function deactivate()
    {
    	$this->active = 0;
    }

    public function activate()
    {
    	$this->active = 1;
    }
}
