<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Order extends Model
{
    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id','id');
    }

    public function orderItems()
    {
    	return $this->hasMany(OrderItem::class);
    }

    public function deliveryAddress()
    {
        return $this->belongsTo(Address::class,'delivery_address_id');
    }
    public function billingAddress()
    {
        return $this->belongsTo(Address::class,'billing_address_id');
    }

    public function updateTaxTotal() {
    	$this->orderItems->each(function($oi) { 
    		$this->tax_total += $oi->tax_amount;
    	});
    	return $this;
    }
    public function updateSubTotal(){
    	$this->orderItems->each(function($oi) { 
    		$this->sub_total += $oi->sub_total;
    	});
    	return $this;
    }
    public function updatePayableAmount(){
    	$this->payable_amount = $this->tax_total + $this->sub_total;	
    	return $this;
    }
    public function setBillingDetails(Address $address)
    {
        $this->billing_address_id = $address->id;
        $this->bill_to = $address->address_to;
        $this->billing_contact = $address->address_contact;
        $this->billing_address_1 = $address->address_line_1;
        $this->billing_address_2 = $address->address_line_2;
        $this->billing_address_city = $address->address_city;
        $this->billing_address_state = $address->address_state;
        $this->billing_address_postcode = $address->address_postcode;
        $this->billing_address_country = $address->address_country;
    }
    public function setDeliveryDetails(Address $address)
    {
        $this->delivery_address_id = $address->id;
        $this->deliver_to = $address->address_to;
        $this->delivery_contact = $address->address_contact;
        $this->delivery_address_1 = $address->address_line_1;
        $this->delivery_address_2 = $address->address_line_2;
        $this->delivery_address_city = $address->address_city;
        $this->delivery_address_state = $address->address_state;
        $this->delivery_address_postcode = $address->address_postcode;
        $this->delivery_address_country = $address->address_country;
    }
    public function copyDeliveryDetailsToBilling()
    {
        $this->billing_address_id = $this->delivery_address_id;
        $this->bill_to = $this->deliver_to;
        $this->billing_contact = $this->delivery_contact;
        $this->billing_address_1 = $this->delivery_address_1;
        $this->billing_address_2 = $this->delivery_address_2;
        $this->billing_address_city = $this->delivery_address_city;
        $this->billing_address_state = $this->delivery_address_state;
        $this->billing_address_postcode = $this->delivery_address_postcode;
        $this->billing_address_country = $this->delivery_address_country;
    }
    public function setOrderDate($orderDate = null)
    {
        $this->order_date = is_null($orderDate) ? Carbon::today() : $orderDate; 
    }
    public function setOrderChannel($channel = null)
    {
        $this->order_channel = is_null($channel) ? 'Web' : $channel;
    }

}
