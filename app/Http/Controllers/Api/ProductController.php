<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tax;
use App\Product; 

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Product $product)
    {
        $product = new Product;
        $product->product_code = $request->get('product_code');
        $product->product_name = $request->get('product_name');
        $product->product_description = $request->get('product_description');
        $product->unit_price = $request->get('unit_price');
        $product->tax_applicable = $request->get('tax_applicable');
        if ($product->tax_applicable) {
            $tax = Tax::find($request->get('tax_id'));
            $product->applyTaxComponent($tax);
        }
        $product->save();
        $product->refresh();
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {
       return Product::find($product->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        
        $product = Product::find($product->id);
        $product->product_code = $request->get('product_code');
        $product->product_name = $request->get('product_name');
        $product->product_description = $request->get('product_description');
        $product->unit_price = $request->get('unit_price');
        $product->tax_applicable = $request->get('tax_applicable');

        if ($product->tax_applicable) {
            $tax = Tax::find($request->get('tax_id'));
            $product->applyTaxComponent($tax);
        }
        $product->save();
        $product->refresh();
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return "Not Implemented Yet";
    }
    public function activate(Request $request, Product $product) {
        $product = Product::find($product->id);
        $product->activate();
        $product->save();
        return $product;
    }
    public function deactivate(Request $request, Product $product) {
        $product = Product::find($product->id);
        $product->deactivate();
        $product->save();
        return $product;
    }
}
