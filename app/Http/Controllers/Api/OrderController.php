<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tax;
use App\Address;
use App\Product; 
use App\Order;
use App\OrderItem;
use App\User;
use App\LookupItem;
use App\Lookup;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return Order::with(['orderItems.product','customer'])->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ------------------------------------------------------------- //
        // Refactor order creation to be done by customer (self service) //
        // ------------------------------------------------------------- //
        // Step 1: Determine whether the Request is initiated by customer or staff
        // Step 2: If request is initiated by customer then 
        //          a. Make the order
        //          b. Set order date
        //          c. Set delivery details and billing details
        //          d. Make order items
        //          e. Save and associate the order with the customer
        //          f. Save and associate the order items to the order
        // Step 3: Raise an event notify the customer. 

        // ------------------------------------------------------------- //
        // Refactor order creation to be done by staff
        // ------------------------------------------------------------- //
        // Step 1: Determine whether the Request is initiated by customer or staff
        // Step 2: If request is initiated by staff then 
        //          a. Make the order
        //          b. Set order date
        //          c. Set delivery details and billing details
        //          d. Make order items
        //          e. Save and associate the order with the customer
        //          f. Save and associate the order items to the order
        // Step 3: Raise an event notify the customer. 

        $customer = User::find(1);
        $order = $this->createNewOrder($request, $customer);
        return Order::with(['orderItems.product','customer'])->find($order->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Order $order)
    {
        return Order::with(['orderItems.product','customer'])->find($order->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $updatedOrder = $this->updateOrder($request,$order);
        return Order::with(['orderItems.product','customer'])->find($order->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createNewOrder(Request $request, User $customer)
    {
        $order = new Order; 
        $order->setOrderDate();
        $deliveryAddress = $customer->getAddress($request->get('delivery_address_id'));
        $billingAddress = $customer->getAddress($request->get('billing_address_id'));
        $order->setDeliveryDetails($deliveryAddress);
        $order->setBillingDetails($billingAddress);
        $order->setOrderChannel($request->get('order_channel'));
        //$order->copyDeliveryDetailsToBilling();
        $items = $this->prepareOrderItems($request->items);
        
        $customer->orders()->save($order);
        $order->orderItems()->saveMany($items);
        $order->updateTaxTotal()
              ->updateSubTotal()
              ->updatePayableAmount()
              ->save();
        return $order;
    }

    public function updateOrder(Request $request, Order $order)
    {
        $customer = User::find($order->customer_id);

        $deliveryAddress = $customer->getAddress($request->get('delivery_address_id'));
        $billingAddress = $customer->getAddress($request->get('billing_address_id'));
        $order->setDeliveryDetails($deliveryAddress);
        $order->setBillingDetails($billingAddress);

        $order->orderItems()->delete();
        $items = $this->prepareOrderItems($request->items);
        $order->orderItems()->saveMany($items);
        $order->updateTaxTotal()
              ->updateSubTotal()
              ->updatePayableAmount()
              ->save();
        return $order;
    }
    public function prepareOrderItems($orderItems)
    {
        return collect($orderItems)->transform(function($item) {
            $orderItem = new OrderItem;
            $product = Product::find($item['product_id']);
            $orderItem->quantity = $item['quantity'];
            $orderItem->product_id = $product->id;
            $orderItem->updateTaxTotal($product)
                      ->updateSubTotal($product);
            return $orderItem;
        });
    }
}
