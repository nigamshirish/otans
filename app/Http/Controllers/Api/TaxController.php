<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tax; 

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tax::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tax = new Tax;
		$tax->name = $request->get('name');
		$tax->percentage = $request->get('percentage');
		$tax->active = 1;
		$tax->save();

		return $tax;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Tax $tax)
    {
        return Tax::find($tax->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tax $tax)
    {
        $tax = Tax::find($tax->id);
		$tax->name = $request->get('name');
		$tax->percentage = $request->get('percentage');
		$tax->active = 1;
		$tax->save();

		return $tax;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return "Not Implemented Yet";
    }

    public function activate(Request $request, Tax $tax) {
    	$tax = Tax::find($tax->id);
		$tax->activate();
		$tax->save();
		return $tax;
    }
    public function deactivate(Request $request, Tax $tax) {
    	$tax = Tax::find($tax->id);
		$tax->deactivate();
		$tax->save();
		return $tax;
    }
}
