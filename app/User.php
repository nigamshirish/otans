<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function addresses()
    {
        return $this->hasMany(Address::class,'customer_id','id');
    }
    public function orders()
    {
        return $this->hasMany(Order::class,'customer_id','id');
    }
    public function getAddress($addressId = null)
    {
        // If addressId is provided then return the matching customer address
        if (!is_null($addressId))
        {
            return $this->addresses->first(function($address) use($addressId) {
                Log::info($address->id .','. $addressId);
                return $address->id === $addressId;
            });
        }
        else
            return $this->getDefaultAddress();
    }
    public function getDefaultAddress()
    {
        return $this->addresses->first(function($a) {
                return $a->is_default == 1;
        });
    }
}
